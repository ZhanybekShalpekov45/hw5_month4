from django.contrib import admin
from . import models

admin.site.register(models.CustomerCloth)
admin.site.register(models.TagCloth)
admin.site.register(models.ProductCloth)
admin.site.register(models.OrderCloth)

