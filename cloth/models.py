from django.db import models


class CustomerCloth(models.Model):
    name = models.CharField('Имя заказчика', max_length=100)
    phone = models.CharField('Номер телефона', max_length=100)

    class Meta:
        verbose_name = 'заказчика'
        verbose_name_plural = 'Заказчики'

    def __str__(self):
        return self.name


class TagCloth(models.Model):
    name_tag = models.CharField('Укажите хэштег', max_length=100)

    class Meta:
        verbose_name = 'тег'
        verbose_name_plural = 'Теги'

    def __str__(self):
        return self.name_tag


class ProductCloth(models.Model):
    title = models.CharField('Название продукта', max_length=100)
    image = models.ImageField('Фото продукта', upload_to='product/')
    price = models.PositiveIntegerField('Цена')
    tag = models.ManyToManyField(TagCloth)

    class Meta:
        verbose_name = 'продукт'
        verbose_name_plural = 'Продукты'

    def __str__(self):
        return self.title


class OrderCloth(models.Model):
    STATUS = (
        ('Ожидание', 'Ожидание'),
        ('Выехал', 'Выехал'),
        ('Доставлен', 'Доставлен'),
    )

    customer = models.ForeignKey(CustomerCloth, on_delete=models.CASCADE)
    product = models.ForeignKey(ProductCloth, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=100, choices=STATUS)

    class Meta:
        verbose_name = 'статус доставки'
        verbose_name_plural = 'статусы доставок'

    def __str__(self):
        return self.status
