from django.urls import path
from . import views

urlpatterns = [
    path('cloth/', views.ProductView.as_view(), name='cloth'),
    path('menshoes/', views.MenShoes.as_view(), name='man'),
    path('womenshoes/', views.WomenShoes.as_view(), name='women'),
    path('childshoes/', views.ChildrenShoes.as_view(), name='children'),
    path('teenshoes/', views.TeenagerShoes.as_view(), name='teenager'),
    path('statusorder/', views.CreatedOrderView.as_view(), name='status')
]
