from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView
from django.views import generic
from . import models,forms
from cloth.models import OrderCloth


class ProductView(ListView):
    template_name = 'cloth/cloth.html'
    queryset = models.ProductCloth.objects.filter().order_by("id")

    def get_tag(self, object):
        return object.tag.name_tag

    def get_queryset(self):
        #return models.ProductCloth.objects.all()
        return models.ProductCloth.objects.filter().order_by("id")
        #return models.ProductCloth.objects.filter(tag__name_tag='Учеба')


class MenShoes(ListView):
    template_name = 'cloth/menshoes.html'
    queryset = models.ProductCloth.objects.filter(tag__name_tag="Мужская одежда")

    def get_queryset(self):
        return models.ProductCloth.objects.filter(tag__name_tag="Женская одежда")


class WomenShoes(ListView):
    template_name = 'cloth/menshoes.html'
    queryset = models.ProductCloth.objects.filter(tag__name_tag="Мужская одежда")

    def get_queryset(self):
        return models.ProductCloth.objects.filter(tag__name_tag="Женская одежда")


class ChildrenShoes(ListView):
    template_name = 'cloth/childshoes.html'
    queryset = models.ProductCloth.objects.filter(tag__name_tag="Мужская одежда")

    def get_queryset(self):
        return models.ProductCloth.objects.filter(tag__name_tag="Женская одежда")


class TeenagerShoes(ListView):
    template_name = 'cloth/teenshoes.html'
    queryset = models.ProductCloth.objects.filter(tag__name_tag="Мужская одежда")

    def get_queryset(self):
        return models.ProductCloth.objects.filter(tag__name_tag="Женская одежда")

class CreatedOrderView(generic.CreateView):
    template_name = 'cloth/status_order.html'
    form_class = forms.StatusOrderForm
    queryset = OrderCloth.objects.all()
    success_url = '/product/'

    def form_valid(self, form):
        print(form.cleaned_data)
        return super(CreatedOrderView).form_valid(form=form)
