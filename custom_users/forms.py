from django import forms
from django.contrib.auth.forms import UserCreationForm
from . import models

USER_TYPE = (
    ('Client', 'Client'),
    ('VipClient', 'VipClient'),
    ('BigBoss', 'BigBoss')
)

GENDER = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other'),
    )

HOME = (
        ('H', 'House'),
        ('F', 'Flat')
    )

STATUS = (
        ('S', 'Single'),
        ('M', 'Married'),
        ('D', 'Divorced'),
        ('W', 'Widowed'),
    )

EDUCATION = (
        ('H', 'High School'),
        ('C', 'College'),
        ('U', 'University'),
        ('G', 'Graduate School'),
        ('P', 'No education'),
    )

class CustomUserRegistration(UserCreationForm):
    birthdate = forms.DateField(required=True)
    email = forms.EmailField(required=True)
    phone_number = forms.CharField(required=True)
    gender = forms.ChoiceField(choices=GENDER, required=True)
    user_type = forms.ChoiceField(choices=USER_TYPE, required=True)
    country = forms.CharField(required=True)
    city = forms.CharField(required=True)
    address = forms.CharField(required=True)
    postal_code = forms.CharField(required=True)
    bank_account = forms.IntegerField(required=True)
    home = forms.ChoiceField(choices=HOME, required=True)
    status = forms.ChoiceField(choices=STATUS, required=True)
    education = forms.ChoiceField(choices=EDUCATION, required=True)
    job_position = forms.CharField(required=True)
    about_me = forms.TextInput()
    profile_photo = forms.ImageField(required=True)
    agree_to_terms = forms.BooleanField(required=True)
    registration_date = forms.TimeField(required=True)

    hobby = forms.CharField(required=False)
    favorite_color = forms.CharField(required=False)
    favorite_food = forms.CharField(required=False)
    language = forms.CharField(required=False)



    class Meta:
        model = models.CustomUser
        fields = (
            'user_type',
            'birthdate',
            'gender',
            'country',
            'city',
            'address',
            'postal_code',
            'phone_number',
            'bank_account',
            'status',
            'education',
            'job_position',
            'about_me',
            'profile_photo',
            'agree_to_terms',
            'registration_date',
            'hobby',
            'favorite_color',
            'favorite_food',
            'language'
        )

    def save(self, commit=True):
        user = super(CustomUserRegistration, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user
