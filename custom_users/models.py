

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class CustomUser(User):
    USER_TYPE = (
        ('Client', 'Client'),
        ('VipClient', 'VipClient'),
        ('BigBoss', 'BigBoss')
    )
    user_type = models.CharField(max_length=100, choices=USER_TYPE)
    birthdate = models.DateField(blank=True, null=True, default="2006-06-30")
    GENDER = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other'),
    )
    gender = models.CharField(max_length=1, choices=GENDER)
    country = models.CharField(max_length=50, default="USA")
    city = models.CharField(max_length=50, default="New-York")
    address = models.CharField(max_length=100, default="Washington h50")
    postal_code = models.CharField(max_length=10, default=10001)
    phone_number = models.CharField(max_length=13, default="+996700561456")
    bank_account = models.IntegerField(null=True)
    HOME = (
        ('H', 'House'),
        ('F', 'Flat')
    )
    home = models.CharField(max_length=1, choices=HOME, null=True)
    STATUS = (
        ('S', 'Single'),
        ('M', 'Married'),
        ('D', 'Divorced'),
        ('W', 'Widowed'),
    )
    status = models.CharField(max_length=1, choices=STATUS, null=False, default="M")
    EDUCATION = (
        ('H', 'High School'),
        ('C', 'College'),
        ('U', 'University'),
        ('G', 'Graduate School'),
        ('P', 'No education'),
    )
    education = models.CharField(max_length=1, choices=EDUCATION,default="U")
    job_position = models.CharField(max_length=50,default="broker")
    about_me = models.TextField(default="I am doctor.I live in a Boston.I have hobby a chess.")
    profile_photo = models.ImageField(upload_to='media',default="tv_show/blackhouse.webp")
    agree_to_terms = models.BooleanField(default=False)
    registration_date = models.DateTimeField(default=timezone.now)

    hobby = models.CharField(max_length=50, blank=True, null=True)
    favorite_color = models.CharField(max_length=50, blank=True, null=True)
    favorite_food = models.CharField(max_length=50, blank=True, null=True)
    language = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.user_type + "-" + self.username
