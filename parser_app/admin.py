from django.contrib import admin
from . import models

admin.site.register(models.TvShowParser)
admin.site.register(models.CarsParser)
