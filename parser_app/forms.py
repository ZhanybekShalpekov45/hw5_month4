from django import forms
from . import parser, models


class ParserForm(forms.Form):
    MEDIA_CHOICES = {
        ('FILM_KG', 'FILMS_KG'),
        ('CAR_KG', 'CARS_KG')
    }
    media_type = forms.ChoiceField(choices=MEDIA_CHOICES)

    class Meta:
        fields = ['media_type']

    def parser_film_data(self):
        if self.data['media_type'] == 'FILM_KG':
            film_parser = parser.parser()
            for i in film_parser:
                models.TvShowParser.objects.create(**i)

    def parser_cars_data(self):
        if self.data['media_type'] == 'CAR_KG':
            car_parser = parser.parser()
            for i in car_parser:
                models.CarsParser.objects.create(**i)

