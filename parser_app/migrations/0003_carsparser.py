# Generated by Django 4.2.4 on 2023-08-29 09:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parser_app', '0002_rename_title_text_tvshowparser_title_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='CarsParser',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('price_usd', models.IntegerField()),
                ('price', models.IntegerField()),
                ('image', models.ImageField(upload_to='')),
            ],
        ),
    ]
