from django.db import models


class TvShowParser(models.Model):
    title_name = models.CharField(max_length=100)
    title_url = models.CharField(max_length=100)
    image = models.ImageField(upload_to='')

    def __str__(self):
        return self.title_name


class CarsParser(models.Model):
    title = models.CharField(max_length=100)
    title_url = models.URLField(null=True)
    price_usd = models.IntegerField()
    price = models.IntegerField()
    image = models.ImageField(upload_to='')

    def __str__(self):
        return self.title
