import requests
from bs4 import BeautifulSoup as BS
from django.views.decorators.csrf import csrf_exempt

URL = 'https://www.mashina.kg'

HEADERS = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36"
}


@csrf_exempt
def get_html(url, params=""):
    req = requests.get(url, headers=HEADERS, params=params)
    return req


@csrf_exempt
def get_data(html):
    soup = BS(html, "html.parser")
    items = soup.find_all("div",class_="search-results-table")
    films = []
    for item in items:
        films.append(
            {"title": item.find("div", class_="block title").get_text(),
             "title_url": URL + item.find("a").get("href"),
             "price": item.find("p", class_="block price").get_text(),
             "image": URL + item.find("div", class_="image-wrap").find("img").get("src"),
             }
        )
    return films


@csrf_exempt
def parser():
    html = get_html(URL)
    if html.status_code == 200:
        films2 = []
        for page in range(0,1):
            html = get_html("https://www.mashina.kg/search/all/",params={'page':page})
            films2.extend(get_data(html.text))
            return films2
    else:
        raise Exception("Не удалось выполнить соединение(")


parser()
