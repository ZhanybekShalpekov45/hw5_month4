from django.urls import path
from . import views

urlpatterns = [
    path('parser_film/', views.ParserFormView.as_view(), name='parse_func'),
    path('parser_film_info/', views.ParserCarsView.as_view(), name='parse_view'),
    path('parser_car/', views.ParserCarsFormView.as_view(),name='parse_car'),
    path('parser_cars_info/', views.ParserCarsView.as_view(), name='parse_car_info')

]