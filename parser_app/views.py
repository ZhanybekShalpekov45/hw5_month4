from django.shortcuts import render
from django.http import HttpResponse
from django.urls import reverse
from django.views.generic import ListView, FormView
from . import models, forms, parser


class ParserView(ListView):
    model = models.TvShowParser
    template_name = 'parser/tv_show_list_parse.html'

    def get_queryset(self):
        return models.TvShowParser.objects.all()


class ParserFormView(FormView):
    template_name = 'parser/start_parsing.html'
    form_class = forms.ParserForm

    def post(self, request,*args,**kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.parser_cars_data()
            return HttpResponse('<h1>Данные взяты...<h1>')

        else:
            return super(ParserFormView, self).post(request, *args, **kwargs)


class ParserCarsView(ListView):
    model = models.CarsParser
    template_name = 'parser/cars_list_parse.html'

    def get_queryset(self):
        return models.CarsParser.objects.all()


class ParserCarsFormView(FormView):
    template_name = 'parser/start_parse_cars.html'
    form_class = forms.ParserForm

    def post(self, request,*args,**kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.parser_cars_data()
            return HttpResponse('<h1>Данные взяты...<h1>')

        else:
            return super(ParserCarsFormView, self).post(request, *args, **kwargs)
