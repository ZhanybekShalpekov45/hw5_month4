from django.urls import path
from . import views

urlpatterns = [
    path('tv_show/', views.TvShowView.as_view(), name='tv_show'),
    path('tv_show/<int:id>/', views.TvShowDetailView.as_view(), name='tv_show_detail'),
    path('tv_show/<int:id>/delete/', views.TvShowDeleteView.as_view(), name='tv_show_delete'),
    path('tv_show/<int:id>/update/', views.UpdateTvShowView.as_view(), name='tv_show_update'),
    path('add-tv_show/', views.CreateTvShowView.as_view(), name='add_tv_show'),
    path('search/', views.Search.as_view(), name='search'),
    path('reviews/', views.ReviewTvShowView.as_view(), name='reviews'),
]

