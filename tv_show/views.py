from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from . import models,forms
from django.views import generic


class TvShowView(generic.ListView):
    template_name = 'tv_show/tv_show.html'
    queryset = models.TvShow.objects.all()

    def get_queryset(self):
        return models.TvShow.objects.all()


class TvShowDetailView(generic.DetailView):
    template_name = 'tv_show/tv_show_detail.html'

    def get_object(self, **kwargs):
        tv_show_id = self.kwargs.get("id")
        return get_object_or_404(models.TvShow, id=tv_show_id)


class CreateTvShowView(generic.CreateView):
    template_name = 'tv_show/crud/add_tv_show.html'
    form_class = forms.Tv_Show_Form
    queryset = models.TvShow.objects.all()
    success_url = '/tv_show/'

    def form_valid(self, form):
        print(form.cleaned_data)
        return super(CreateTvShowView, self).form_valid(form=form)


class TvShowDeleteView(generic.DeleteView):
    template_name = 'tv_show/crud/confirm_delete.html'
    success_url = '/tv_show/'

    def get_object(self, **kwargs):
        tv_show_id = self.kwargs.get('id')
        return get_object_or_404(models.TvShow, id=tv_show_id)


class UpdateTvShowView(generic.UpdateView):
    template_name = 'tv_show/crud/update_tv_show.html'
    form_class = forms.Tv_Show_Form
    success_url = '/tv_show/'

    def get_object(self, **kwargs):
        tv_show_id = self.kwargs.get('id')
        return get_object_or_404(models.TvShow, id=tv_show_id)

    def form_valid(self, form):
        return super(UpdateTvShowView, self).form_valid(form=form)


class Search(generic.ListView):
    template_name = "tv_show/tv_show.html"
    context_object_name = "tv_show"
    paginate_by = 5

    def get_queryset(self):
        return models.TvShow.objects.filter(
            title__icontains=self.request.GET.get("q")
        )

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context["q"] = self.request.GET.get("q")
        return context


class ReviewTvShowView(generic.CreateView):
    template_name = 'tv_show/Reviews.html'
    form_class = forms.ReviewForm
    queryset = models.TvShow.objects.all()
    success_url = '/tv_show/'

    def form_valid(self, form):
        print(form.cleaned_data)
        tv_show = self.get_object()
        review = form.save(commit=False)
        review.tv_show_choice = tv_show
        review.save()
        return super(ReviewTvShowView, self).form_valid(form=form)
# def tv_show_view(request):
#     tv_show = models.TvShow.objects.all()
#     return render(request, 'tv_show/tv_show.html', {'tv_show': tv_show})
#
#
# def tv_show_detail_view(request, id):
#     tv_show_id = get_object_or_404(models.TvShow, id=id)
#     return render(request, 'tv_show/tv_show_detail.html',
#                   {'tv_show_id': tv_show_id})
#
#
# #Добавление объекта через формы CRUD
# def create_tv_show_view(request):
#     method = request.method
#     if method == "POST":
#         form = forms.Tv_Show_Form(request.POST, request.FILES)
#         if form.is_valid():
#             form.save()
#             return HttpResponse("Тв-шоу успешно добавлено в БД")
#     else:
#         form = forms.Tv_Show_Form()
#
#     return render(request, 'tv_show/crud/add_tv_show.html',
#                   {"form": form})
#
#
# #Удаление из БД CRUD
# def delete_tv_show_view(request, id):
#     tv_show_id = get_object_or_404(models.TvShow, id=id)
#     tv_show_id.delete()
#     return HttpResponse('Тв-шоу успешно удалено из БД')
#
# #Изменение объектов в CRUD
#
#
# def update_tv_show_view(request,id):
#     tv_show_id = get_object_or_404(models.TvShow, id=id)
#     if request.method == "POST":
#         form = forms.Tv_Show_Form(instance=tv_show_id, data=request.POST)
#         if form.is_valid():
#             form.save()
#             return HttpResponse('Успешно обновлено')
#     else:
#         form = forms.Tv_Show_Form(instance=tv_show_id)
#
#     context = {
#         'form': form,
#         'tv_show_id': tv_show_id
#     }
#     return render(request, 'tv_show/crud/update_tv_show.html', context)
#
#
